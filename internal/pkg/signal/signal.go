package signal

type Signal interface {
	AmplitudeAt(seconds Seconds) Decibel
}
