package signal

type additiveSignal struct {
	signals []Signal
}

func NewAdditiveSignal(signals ...Signal) Signal {
	return &additiveSignal{signals: signals}
}

func (s *additiveSignal) AmplitudeAt(seconds Seconds) Decibel {
	cumulativePower := Decibel(0)
	for _, signal := range s.signals {
		cumulativePower += signal.AmplitudeAt(seconds)
	}
	return cumulativePower
}
