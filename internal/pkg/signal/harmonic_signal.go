package signal

import (
	"math"
)

type harmonicSignal struct {
	amplitude Decibel
	frequency Hz
}

func NewHarmonicSignal(amplitude Decibel, frequency Hz) Signal {
	return &harmonicSignal{
		amplitude: amplitude,
		frequency: frequency,
	}
}

func (hs *harmonicSignal) AmplitudeAt(seconds Seconds) Decibel {
	sin := math.Sin(2 * math.Pi * float64(hs.frequency) * float64(seconds))

	return Decibel(float64(hs.amplitude) * sin)
}
