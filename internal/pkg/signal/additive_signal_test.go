package signal_test

import (
	"github.com/stretchr/testify/assert"
	"test-task/internal/pkg/signal"
	"testing"
)

func TestAdditiveSignalAmplitudeShouldBeSumOfAmplitudes(t *testing.T) {
	harmonicSignal1 := signal.NewHarmonicSignal(5, 0.25)
	harmonicSignal2 := signal.NewHarmonicSignal(0.2, 10)
	harmonicSignal3 := signal.NewHarmonicSignal(2.3, 4.8)
	seconds := signal.Seconds(1.25)

	additiveSignal := signal.NewAdditiveSignal(harmonicSignal1, harmonicSignal2, harmonicSignal3)
	signalAmplitude := additiveSignal.AmplitudeAt(seconds)

	expectedAmplitude := harmonicSignal1.AmplitudeAt(seconds) + harmonicSignal2.AmplitudeAt(seconds) + harmonicSignal3.AmplitudeAt(seconds)

	assert.InDelta(t, float64(expectedAmplitude), float64(signalAmplitude), 1e-8)
}
