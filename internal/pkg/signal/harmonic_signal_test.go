package signal_test

import (
	"github.com/stretchr/testify/assert"
	"test-task/internal/pkg/signal"
	"testing"
)

type harmonicSignalAmplitudeAtTest struct {
	maxAmplitude signal.Decibel
	frequency    signal.Hz
	seconds      signal.Seconds

	expectedAmplitudeAtSeconds signal.Decibel
}

var harmonicSignalAmplitudeAtTests = []harmonicSignalAmplitudeAtTest{
	{maxAmplitude: 1, frequency: 1, seconds: 1, expectedAmplitudeAtSeconds: 0},       // sin(2*pi)
	{maxAmplitude: 5, frequency: 0.25, seconds: 1, expectedAmplitudeAtSeconds: 5},    // 5*sin(pi/2)
	{maxAmplitude: 2.5, frequency: 10, seconds: 0.05, expectedAmplitudeAtSeconds: 0}, // 2.5*sin(pi)
}

func TestHarmonicSignalAmplitudeShouldBeSinusoid(t *testing.T) {
	for _, test := range harmonicSignalAmplitudeAtTests {
		harmonicSignal := signal.NewHarmonicSignal(test.maxAmplitude, test.frequency)
		amplitudeAtSeconds := harmonicSignal.AmplitudeAt(test.seconds)

		assert.InDelta(t, float64(test.expectedAmplitudeAtSeconds), float64(amplitudeAtSeconds), 1e-8)
	}
}
