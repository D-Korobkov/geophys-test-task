package signal

type Seconds float64
type Decibel float64
type Hz float64
