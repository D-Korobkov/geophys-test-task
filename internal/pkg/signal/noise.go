package signal

import (
	"math/rand"
)

type DeterministicNoise struct {
	generator  *rand.Rand
	lowerBound Decibel
	upperBound Decibel
}

func NewDeterministicNoise(lowerBound Decibel, upperBound Decibel) Signal {
	return &DeterministicNoise{
		generator:  rand.New(rand.NewSource(0)),
		lowerBound: lowerBound,
		upperBound: upperBound,
	}
}

func (n *DeterministicNoise) AmplitudeAt(_ Seconds) Decibel {
	return Decibel(n.generator.Float64())*(n.upperBound-n.lowerBound) + n.lowerBound
}
