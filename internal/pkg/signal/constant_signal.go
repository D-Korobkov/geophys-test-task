package signal

type constantSignal struct {
	amplitude Decibel
}

func NewConstantSignal(amplitude Decibel) Signal {
	return &constantSignal{amplitude: amplitude}
}

func (s *constantSignal) AmplitudeAt(_ Seconds) Decibel {
	return s.amplitude
}
