package fourier

import "math"

func DiscreteFourierTransform(xs []complex128) []complex128 {
	return discreteFourierTransformImpl(xs, false)
}

func IDiscreteFourierTransform(ys []complex128) []complex128 {
	return discreteFourierTransformImpl(ys, true)
}

func discreteFourierTransformImpl(xs []complex128, inverseFlag bool) []complex128 {
	xsSize := len(xs)

	ys := make([]complex128, xsSize)
	for yIdx := range ys {
		y := &ys[yIdx]
		for xIdx, x := range xs {
			sin, cos := math.Sincos(2 * math.Pi * float64(xIdx) * float64(yIdx) / float64(xsSize))
			if inverseFlag {
				*y += x * complex(cos, sin)
			} else {
				*y += x * complex(cos, -sin)
			}
		}
		if inverseFlag {
			*y /= complex(float64(xsSize), 0)
		}
	}

	return ys
}
