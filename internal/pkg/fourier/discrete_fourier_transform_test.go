package fourier_test

import (
	"github.com/stretchr/testify/assert"
	"test-task/internal/pkg/fourier"
	"testing"
)

type dftTest struct {
	inRe  []float64
	inIm  []float64
	outRe []float64
	outIm []float64
}

var dftTests = []dftTest{
	{
		inRe:  []float64{1, 0, 0, 0},
		inIm:  []float64{0, 0, 0, 0},
		outRe: []float64{1, 1, 1, 1},
		outIm: []float64{0, 0, 0, 0},
	},
	{
		inRe:  []float64{0, 1, 0, 0},
		inIm:  []float64{0, 0, 0, 0},
		outRe: []float64{1, 0, -1, 0},
		outIm: []float64{0, -1, 0, 1},
	},
	{
		inRe:  []float64{1, 0, 0, 0, 0},
		inIm:  []float64{0, 0, 0, 0, 0},
		outRe: []float64{1, 1, 1, 1, 1},
		outIm: []float64{0, 0, 0, 0, 0},
	},
	{
		inRe:  []float64{1, 1, 1},
		inIm:  []float64{0, 0, 0},
		outRe: []float64{3, 0, 0},
		outIm: []float64{0, 0, 0},
	},
	{
		inRe:  []float64{-1, 2, 2, 105},
		inIm:  []float64{0, 0, 0, 0},
		outRe: []float64{108, -3, -106, -3},
		outIm: []float64{0, 103, 0, -103},
	},
}

func TestDiscreteFourierTransform(t *testing.T) {
	for _, test := range dftTests {
		var in []complex128
		for idx := range test.inRe {
			in = append(in, complex(test.inRe[idx], test.inIm[idx]))
		}

		transformResult := fourier.DiscreteFourierTransform(in)

		var transformResultRe, transformResultIm []float64
		for _, complexOut := range transformResult {
			transformResultRe = append(transformResultRe, real(complexOut))
			transformResultIm = append(transformResultIm, imag(complexOut))
		}

		assert.InDeltaSlice(t, test.outRe, transformResultRe, 1e-8)
		assert.InDeltaSlice(t, test.outIm, transformResultIm, 1e-8)
	}
}

func TestIDiscreteFourierTransform(t *testing.T) {
	for _, test := range dftTests {
		var in []complex128
		for idx := range test.inRe {
			in = append(in, complex(test.inRe[idx], test.inIm[idx]))
		}

		transformResult := fourier.IDiscreteFourierTransform(fourier.DiscreteFourierTransform(in))

		var transformResultRe, transformResultIm []float64
		for _, complexOut := range transformResult {
			transformResultRe = append(transformResultRe, real(complexOut))
			transformResultIm = append(transformResultIm, imag(complexOut))
		}

		assert.InDeltaSlice(t, test.inRe, transformResultRe, 1e-8)
		assert.InDeltaSlice(t, test.inIm, transformResultIm, 1e-8)
	}
}
