package series

import (
	"github.com/stretchr/testify/assert"
	"test-task/internal/pkg/signal"
	"testing"
	"time"
)

func TestNewTimeSeriesFromSignalShouldCollectAmplitudesWithTheGivenSampleRate(t *testing.T) {
	sampleRate := signal.Hz(4)
	duration := 2 * time.Second
	constSignal := signal.NewConstantSignal(24)

	timeSeries := NewTimeSeriesFromSignal(constSignal, sampleRate, duration)

	expectedTimeSeries := TimeSeries{
		amplitudesDb:    []float64{24, 24, 24, 24, 24, 24, 24, 24},
		sampleRateHz:    4,
		durationSeconds: 2,
	}
	assert.Equal(t, expectedTimeSeries, *timeSeries)
}
