package series

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIdealLowFrequencyFilterShouldRemoveHighFrequencies(t *testing.T) {
	series := FrequencySeries{
		amplitudesDb: []complex128{
			complex(1, 1),
			complex(2, 2),
			complex(3, 3),
			complex(2, 2),
			complex(1, 1),
		},
		sampleRateHz:    5,
		durationSeconds: 1,
	}

	filteredSeries := series.ApplyIdealLowFrequencyFilter(1.5)

	expectedSeries := FrequencySeries{
		amplitudesDb: []complex128{
			complex(1, 1),
			complex(2, 2),
			complex(0, 0),
			complex(2, 2),
			complex(1, 1),
		},
		sampleRateHz:    5,
		durationSeconds: 1,
	}
	assert.Equal(t, expectedSeries, *filteredSeries)
}
