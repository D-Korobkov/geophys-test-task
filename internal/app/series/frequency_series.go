package series

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"math/cmplx"
	"test-task/internal/pkg/fourier"
)

type FrequencySeries struct {
	amplitudesDb    []complex128
	sampleRateHz    float64
	durationSeconds float64
}

func (fs *FrequencySeries) ToTimeSeries() *TimeSeries {
	complexAmplitudesDb := fourier.IDiscreteFourierTransform(fs.amplitudesDb)
	amplitudesDb := make([]float64, len(complexAmplitudesDb))
	for idx, amplitude := range complexAmplitudesDb {
		amplitudesDb[idx] = real(amplitude)
	}

	return &TimeSeries{
		amplitudesDb:    amplitudesDb,
		sampleRateHz:    fs.sampleRateHz,
		durationSeconds: fs.durationSeconds,
	}
}

func (fs *FrequencySeries) ApplyIdealLowFrequencyFilter(thresholdFrequencyHz float64) *FrequencySeries {
	filteredAmplitudesDb := make([]complex128, len(fs.amplitudesDb))
	for idx := 0; idx < len(filteredAmplitudesDb); idx++ {
		frequency := float64(idx) / fs.durationSeconds
		if frequency <= thresholdFrequencyHz {
			filteredAmplitudesDb[idx] = fs.amplitudesDb[idx]
			filteredAmplitudesDb[len(filteredAmplitudesDb)-idx-1] = fs.amplitudesDb[len(filteredAmplitudesDb)-idx-1]
		}
	}

	return &FrequencySeries{
		amplitudesDb:    filteredAmplitudesDb,
		sampleRateHz:    fs.sampleRateHz,
		durationSeconds: fs.durationSeconds,
	}
}

func (fs *FrequencySeries) Print(title string, file string) error {
	xys := make(plotter.XYs, len(fs.amplitudesDb))
	for idx, amplitude := range fs.amplitudesDb {
		xys[idx].X = float64(idx) / fs.durationSeconds
		xys[idx].Y = cmplx.Abs(amplitude) / fs.sampleRateHz
	}
	line, err := plotter.NewLine(xys)
	if err != nil {
		return err
	}

	p := plot.New()
	p.Title.Text = title
	p.X.Label.Text = "Frequency (Hz)"
	p.X.Width = vg.Points(1)
	p.Y.Label.Text = "Amplitude (dB)"
	p.Y.Width = vg.Points(1)
	p.Add(line)

	return p.Save(vg.Points(1080), vg.Points(720), file)
}
