package series

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"test-task/internal/pkg/fourier"
	"test-task/internal/pkg/signal"
	"time"
)

type TimeSeries struct {
	amplitudesDb    []float64
	sampleRateHz    float64
	durationSeconds float64
}

func NewTimeSeriesFromSignal(originalSignal signal.Signal, sampleRate signal.Hz, duration time.Duration) *TimeSeries {
	var amplitudesDb []float64
	for secs := signal.Seconds(0); secs < signal.Seconds(duration.Seconds()); secs += signal.Seconds(1 / float64(sampleRate)) {
		amplitude := originalSignal.AmplitudeAt(secs)
		amplitudesDb = append(amplitudesDb, float64(amplitude))
	}

	return &TimeSeries{
		amplitudesDb:    amplitudesDb,
		sampleRateHz:    float64(sampleRate),
		durationSeconds: duration.Seconds(),
	}
}

func (ts *TimeSeries) ToFrequencySeries() *FrequencySeries {
	complexAmplitudesDb := make([]complex128, len(ts.amplitudesDb))
	for idx, amplitude := range ts.amplitudesDb {
		complexAmplitudesDb[idx] = complex(amplitude, 0)
	}

	return &FrequencySeries{
		amplitudesDb:    fourier.DiscreteFourierTransform(complexAmplitudesDb),
		sampleRateHz:    ts.sampleRateHz,
		durationSeconds: ts.durationSeconds,
	}
}

func (ts *TimeSeries) Print(title string, file string) error {
	xys := make(plotter.XYs, len(ts.amplitudesDb))
	for idx, amplitude := range ts.amplitudesDb {
		xys[idx].X = float64(idx) / ts.sampleRateHz
		xys[idx].Y = amplitude
	}
	line, err := plotter.NewLine(xys)
	if err != nil {
		return err
	}

	p := plot.New()
	p.Title.Text = title
	p.X.Label.Text = "Time (seconds)"
	p.X.Width = vg.Points(1)
	p.Y.Label.Text = "Amplitude (dB)"
	p.Y.Width = vg.Points(1)
	p.Add(line)

	return p.Save(vg.Points(1080), vg.Points(720), file)
}
