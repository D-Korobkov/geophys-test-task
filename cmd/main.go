package main

import (
	"test-task/internal/app/series"
	"test-task/internal/pkg/signal"
	"time"
)

func main() {
	inputSignal := signal.NewAdditiveSignal(
		signal.NewHarmonicSignal(1, 5),
		signal.NewHarmonicSignal(0.5, 20),
		signal.NewDeterministicNoise(-0.3, 0.3),
	)

	signalTimeSeries := series.NewTimeSeriesFromSignal(inputSignal, 1000, time.Second)
	signalTimeSeries.Print("Сигнал", "input.png")

	signalFrequencySeries := signalTimeSeries.ToFrequencySeries()
	filteredSignalFrequencySeries := signalFrequencySeries.ApplyIdealLowFrequencyFilter(20)

	filteredSignalTimeSeries := filteredSignalFrequencySeries.ToTimeSeries()
	filteredSignalTimeSeries.Print("Сигнал после применения идеального НЧ фильтра", "output.png")
}
